Async API Gateway
=================

This project shows how to combine two backend JSON API responses to one combined JSON response.
Processing of the web request and requesting the two API endpoints from the backend is done
in an asynchronous manner.

Building and starting the app
-----------------------------

You will need a Java 8 SDK and at least Maven 3.2.X for building and running the app. 

Go to the projects root folder and build the app with maven.

```
$ mvn clean package
```

Run the app with maven like this
  
```
$ mvn spring-boot:run
```


Usage
-----

This app combines the json of following two api endpoint.

```
http://jsonplaceholder.typicode.com/users/{userId}
http://jsonplaceholder.typicode.com/posts?userId={userId}
```

First api endpoint returns some user data, second returns some posts data
a user has written.
 
Have a look at the api result for both endpoints. Lets explore the 
data for one specific user.
 
http://jsonplaceholder.typicode.com/users/1
http://jsonplaceholder.typicode.com/posts?userId=1

If you have started the app, use following url to request a 
combined json result for the user.

http://localhost:8080/userwithposts/1


Thoughts about coding and testing
---------------------------------

1. Endpoint `jsonplaceholder.typicode.com` always returned 403 when using springs rest template.
Therefore I switch to plain Apache HTTP client lib. This is strange! Maybe some Spring security 
configuration is missing or `jsonplaceholder.typicode.com` deny requests done by the spring rest template :-).
I documented this behaviour with test `RestTemplateApiClientTest`.
 
2. The web container is processing requests in an asynchronous manner. You can use interface 
 java SDK interface `Callable` or Spring's class `DeferredResult`.
 I selected the fancy `DeferredResult` since you have more features like
 using your own threadpool or working with `CompletableFuture`. 
 
 https://www.javacodegeeks.com/2015/07/understanding-callable-and-spring-deferredresult.html
 
 http://www.codebulb.ch/2015/07/completablefuture-clean-callbacks-with-java-8s-promises-part-1.html

3. I hope that testing is sufficient for ensuring that the app works in an asynchronous manner.

4. The app is using json library 'json-simple' https://code.google.com/archive/p/json-simple/.
Since the app should only merge some raw json results, it is not necessary to use a real object mapper
like the 'jackson json' library.
 
 
TODOs
-----

1. Of course, the app needs some error handling. Use a JSON responses for errors,
e.g. when requesting the app without user id http://localhost:8080/userwithposts/

2. Class 'ApiClient' implements some common http requesting. Since class
'JSONObject' and class 'JSONArray' does not derive from any 
common useful parent class and does not implement any useful common interface, 
 we have to implement GETing a json hash and a json array as response root separately.

3. Use some monitoring and fallback mechanism in case the `jsonplaceholder.typicode.com` 
endpoints are not working.
e.g. Netflix Hystrix (see http://projects.spring.io/spring-cloud/)