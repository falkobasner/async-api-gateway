package de.demo.platform;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.util.StopWatch;

import java.io.InputStreamReader;
import java.nio.charset.Charset;

import static org.springframework.http.HttpHeaders.ACCEPT;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@Slf4j
public class ApiClient {
	private final CloseableHttpClient httpClient = HttpClientBuilder.create().build();

	public JSONObject getJsonObject(final String url) {
		try {
			final StopWatch stopWatch = new StopWatch(url);
			stopWatch.start();
			final JSONObject response = doGetJsonObject(url);
			stopWatch.stop();

			log.info(stopWatch.shortSummary());

			return response;
		} catch (Exception e) {
			log.error("error while requesting api", e);
			throw new ApiClientException(e);
		}
	}

	@SneakyThrows
	JSONObject doGetJsonObject(final String url) {
		final CloseableHttpResponse response = httpClient.execute(createHttpGet(url));

		try {
			// TODO it seems that the parser is not threadsafe, therefore create one for every response
			return (JSONObject) new JSONParser().parse(new InputStreamReader(response.getEntity().getContent(), Charset.forName("UTF-8")));
		} finally {
			response.close();
		}
	}

	public JSONArray getJsonArray(final String url) {
		try {
			final StopWatch stopWatch = new StopWatch(url);
			stopWatch.start();
			final JSONArray response = doGetJsonArray(url);
			stopWatch.stop();
			log.info(stopWatch.shortSummary());

			return response;
		} catch (Exception e) {
			log.error("error while requesting api", e);
			throw new ApiClientException(e);
		}
	}

	@SneakyThrows
	JSONArray doGetJsonArray(final String url) {
		final CloseableHttpResponse response = httpClient.execute(createHttpGet(url));

		try {
			// TODO it seems that the parser is not threadsafe, therefore create one for every response
			return (JSONArray) new JSONParser().parse(new InputStreamReader(response.getEntity().getContent(), Charset.forName("UTF-8")));
		} finally {
			response.close();
		}
	}

	private HttpGet createHttpGet(String url) {
		HttpGet httpGet = new HttpGet(url);
		httpGet.addHeader(ACCEPT, APPLICATION_JSON_UTF8_VALUE);
		return httpGet;
	}
}
