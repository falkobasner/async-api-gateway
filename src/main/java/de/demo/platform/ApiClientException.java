package de.demo.platform;

public class ApiClientException extends RuntimeException {
    public ApiClientException(final Throwable cause) {
        super(cause);
    }
}
