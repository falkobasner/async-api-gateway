package de.demo.platform;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StopWatch;
import org.springframework.web.client.RestTemplate;

import static org.springframework.http.HttpHeaders.ACCEPT;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@Slf4j
@Component
public class RestTemplateApiClient {
	private final RestTemplate restTemplate;
	private final MultiValueMap headers;
	private final ObjectMapper mapper = new ObjectMapper();

	@Autowired
	public RestTemplateApiClient(final RestTemplate restTemplate) {
		this.restTemplate = restTemplate;

		this.headers = new LinkedMultiValueMap<String, String>();
		headers.add(ACCEPT, APPLICATION_JSON_UTF8_VALUE);
	}

	public JsonNode getJson(final String url) {
		try {
			final StopWatch stopWatch = new StopWatch(url);
			stopWatch.start();
			final JsonNode response = doGetJson(url);
			stopWatch.stop();

			log.info(stopWatch.shortSummary());

			return response;
		} catch (Exception e) {
			log.error("error while requesting api", e);
			throw new ApiClientException(e);
		}
	}

	@SneakyThrows
	JsonNode doGetJson(final String url) {
		final ResponseEntity<String> response =
				restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), String.class);
		return mapper.readTree(response.getBody());
	}

}
