package de.demo.posts;

import de.demo.platform.ApiClient;
import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;

import static java.util.concurrent.CompletableFuture.completedFuture;

@Component
public class UserPostsApiClient extends ApiClient {
    private final UserPostsApiProperties properties;

    @Autowired
    public UserPostsApiClient(final UserPostsApiProperties properties) {
        this.properties = properties;
    }

    @Async
    public CompletableFuture<JSONArray> getUserPosts(final long userId) {
        return completedFuture(getJsonArray(properties.getUrl() + "/?userId=" + userId));
    }
}
