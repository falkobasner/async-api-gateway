package de.demo.user;

import de.demo.platform.ApiClient;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;

import static java.util.concurrent.CompletableFuture.completedFuture;

@Component
public class UserApiClient extends ApiClient {
	private final UserApiProperties properties;

	@Autowired
	public UserApiClient(final UserApiProperties properties) {
		this.properties = properties;
	}

	@Async
	public CompletableFuture<JSONObject> getUser(final long userId) {
		return completedFuture(getJsonObject(properties.getUrl() + "/" + userId));
	}
}
