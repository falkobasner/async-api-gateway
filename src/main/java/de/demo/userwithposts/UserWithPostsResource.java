package de.demo.userwithposts;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.Map;

@Slf4j
@RestController
public class UserWithPostsResource {
	private final UserWithPostsService userWithPostsService;

	@Autowired
	public UserWithPostsResource(final UserWithPostsService userWithPostsService) {
		this.userWithPostsService = userWithPostsService;
	}

	@RequestMapping(value = "/userwithposts/{userId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public DeferredResult<Map<String, Object>> getUserWithPosts(@PathVariable("userId") final Long userId) {

        final StopWatch stopWatch = new StopWatch("request user with posts, userId=" + userId.toString());
        stopWatch.start();

		final DeferredResult<Map<String, Object>> result = new DeferredResult<>();

		log.info("request received");

		userWithPostsService.getUserWithPosts(userId).handle((combined, ex) -> {
            stopWatch.stop();
            log.info(stopWatch.shortSummary());
            return result.setResult(combined);
        });

		log.info("request thread released");

		return result;
	}

}
