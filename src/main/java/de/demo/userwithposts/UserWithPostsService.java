package de.demo.userwithposts;

import de.demo.posts.UserPostsApiClient;
import de.demo.user.UserApiClient;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;

@Slf4j
@Component
public class UserWithPostsService {

	static JSONObject combine(final JSONObject user, final JSONArray posts) {
        log.info("combine user and posts");
		final JSONObject combined = new JSONObject();
		combined.put("user", user);
		combined.put("posts", posts);
		return combined;
	}

	private final UserApiClient userApiClient;
	private final UserPostsApiClient userPostsApiClient;

	@Autowired
	public UserWithPostsService(final UserApiClient userApiClient, final UserPostsApiClient userPostsApiClient) {
		this.userApiClient = userApiClient;
		this.userPostsApiClient = userPostsApiClient;
	}

	@Async
	@SneakyThrows
	public CompletableFuture<JSONObject> getUserWithPosts(final long userId) {
		log.info("request user and posts");
		return userApiClient.getUser(userId)
                .thenCombine(userPostsApiClient.getUserPosts(userId), UserWithPostsService::combine);
	}
}
