package de.demo.platform;

import de.demo.Application;
import org.json.simple.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
public class ApiClientIT {
    public static final long USER_ID = 1L;
    public static final String URL = "http://jsonplaceholder.typicode.com/users/" + USER_ID;
    public static final String URL_WITH_INVALID_HOST = "http://UPS.jsonplaceholder.typicode.com/users/" + USER_ID;

    private ApiClient apiClient = new ApiClient();

    @Test
    public void getJsonObject() {
        final JSONObject json = apiClient.getJsonObject(URL);

        assertThat(json.containsKey("id"), is(true));
        assertThat(json.get("id"), is(USER_ID));
    }

    @Test(expected = ApiClientException.class)
    public void getJsonObject_With_Unknown_Host() {
        apiClient.getJsonObject(URL_WITH_INVALID_HOST);
    }
}