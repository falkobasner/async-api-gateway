package de.demo.platform;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockserver.client.server.MockServerClient;
import org.mockserver.junit.MockServerRule;

import static java.lang.String.format;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

public class ApiClientTest {
	public static final long USER_ID = 4711L;
	public static final String JSON_OBJECT_TEMPLATE = "{\"id\":%s}";
	public static final String JSON_ARRAY_TEMPLATE = "[{\"id\":%s}]";

	@Rule
	public MockServerRule mockServerRule = new MockServerRule(this);
	private MockServerClient mockServerClient =
			new MockServerClient("localhost", mockServerRule.getPort());

	private ApiClient apiClient = new ApiClient();
	final String url = "http://localhost:" + mockServerRule.getPort() + "/json";

	@Before
	public void setup()  {
		mockServerClient.reset();
	}

	@Test
	public void getJsonObject() {
		mockServerClient
                .when(request().withMethod("GET"))
			    .respond(response().withBody(format(JSON_OBJECT_TEMPLATE, USER_ID)).withStatusCode(200));

		final JSONObject json = apiClient.getJsonObject(url);

		assertThat(json.containsKey("id"), is(true));
		assertThat(json.get("id"), is(USER_ID));
	}

	@Test
	public void getJsonArray() {
		mockServerClient
                .when(request().withMethod("GET"))
			    .respond(response().withBody(format(JSON_ARRAY_TEMPLATE, USER_ID)).withStatusCode(200));

		final JSONArray json = apiClient.getJsonArray(url);

		assertThat(json.size(), is(1));

		JSONObject jsonObject = (JSONObject) json.get(0);
		assertThat(jsonObject.containsKey("id"), is(true));
		assertThat(jsonObject.get("id"), is(USER_ID));
	}

}