package de.demo.platform;

import com.fasterxml.jackson.databind.JsonNode;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


public class RestTemplateApiClientTest {
	public static final long USER_ID = 1L;

	public static final String URL = "http://jsonplaceholder.typicode.com/users/" + USER_ID;
	public static final String URL_WITH_INVALID_HOST = "http://UPS.jsonplaceholder.typicode.com/users/" + USER_ID;

    public static final String OTHER_URL = "http://echo.jsontest.com/key/value/one/two";
    public static final String OTHER_URL_WITH_INVALID_HOST = "http://UPS.echo.jsontest.com/key/value/one/two";


    private RestTemplateApiClient apiClient = new RestTemplateApiClient(new RestTemplate());

    @Ignore("'typicode.com' return 403 here for the rest template")
    @Test
    public void getJson_From_Typicode() {
        final JsonNode json = apiClient.getJson(URL);
        assertThat(json.get("id"), is(USER_ID));
    }

    @Test(expected = ApiClientException.class)
    public void getJsonObject_From_Typicode_With_Unknown_Host() {
        apiClient.getJson(URL_WITH_INVALID_HOST);
    }

	@Test
	public void getJsonObject() {
		final JsonNode json = apiClient.getJson(OTHER_URL);
		assertThat(json.get("one").asText(), is("two"));
	}

	@Test(expected = ApiClientException.class)
	public void getJsonObject_With_Unknown_Host() {
		apiClient.getJson(OTHER_URL_WITH_INVALID_HOST);
	}
}