package de.demo.posts;

import de.demo.Application;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.IntStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
public class UserPostsApiClientIT {
    public static final long USER_ID = 1L;
    public static final long UNKNOWN_USER_ID = 4711L;
    public static final String JSON_KEY_USER_ID = "userId";

    @Autowired
    public UserPostsApiClient apiClient;

    @Test
    public void getUserPostsJson() throws ExecutionException, InterruptedException {
        final Future<JSONArray> jsonFuture = apiClient.getUserPosts(USER_ID);
        assertThat(jsonFuture.isDone(), is(false));

        final List<JSONObject> posts = new LinkedList<>();

        final JSONArray postsJson = jsonFuture.get();
        IntStream.range(0, postsJson.size()).forEach( ii -> posts.add((JSONObject) postsJson.get(ii)) );

        posts.stream().forEach( p -> assertThat(p.get(JSON_KEY_USER_ID), is(USER_ID)));
    }

    @Test
    public void getUserPostsJson_With_Unknown_Id() throws ExecutionException, InterruptedException {
        final Future<JSONArray> jsonFuture = apiClient.getUserPosts(UNKNOWN_USER_ID);
        assertThat(jsonFuture.isDone(), is(false));

        assertThat(jsonFuture.get().isEmpty(), is(true));
    }
}