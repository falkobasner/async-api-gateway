package de.demo.posts;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockserver.client.server.MockServerClient;
import org.mockserver.junit.MockServerRule;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.IntStream;

import static java.lang.String.format;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

public class UserPostsApiClientTest {
	public static final String URL_TEMPLATE = "http://localhost:%s.com";
    public static final String KEY_USER_ID = "userId";
    public static final Long USER_ID = 111L;
    public static final String JSON_ARRAY_TEMPLATE = "[{\"userId\":%s}]";

    private UserPostsApiProperties properties = new UserPostsApiProperties();
	public UserPostsApiClient client;

	@Rule
	public MockServerRule mockServerRule = new MockServerRule(this);
	private MockServerClient mockServerClient = new MockServerClient("localhost", mockServerRule.getPort());

	@Before
	public void setup() {
		properties.setUrl(format(URL_TEMPLATE, mockServerRule.getPort()));
		client = new UserPostsApiClient(properties);
		mockServerClient.reset();
	}

	@Test
	public void getUserPosts() throws ExecutionException, InterruptedException {
		mockServerClient.when(request()
                .withQueryStringParameter(KEY_USER_ID, USER_ID.toString())
                .withMethod("GET"))
			.respond(response().withBody(format(JSON_ARRAY_TEMPLATE, USER_ID)).withStatusCode(200));

		final CompletableFuture<JSONArray> postsFuture = client.getUserPosts(USER_ID);

		final JSONArray postsJson = postsFuture.get();
		final List<JSONObject> posts = new LinkedList<>();
		IntStream.range(0, postsJson.size()).forEach(ii -> posts.add((JSONObject) postsJson.get(ii)));

		posts.stream().forEach(p -> assertThat(p.get("userId"), is(USER_ID)));
	}
}