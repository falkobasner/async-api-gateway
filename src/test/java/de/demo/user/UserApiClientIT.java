package de.demo.user;

import de.demo.Application;
import org.json.simple.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
public class UserApiClientIT {
    public static final long USER_ID = 1L;
    public static final long UNKNOWN_USER_ID = 4711L;

    @Autowired
    public UserApiClient apiClient;

    @Test
    public void getUserJson() throws ExecutionException, InterruptedException {
        final Future<JSONObject> jsonFuture = apiClient.getUser(USER_ID);
        assertThat(jsonFuture.isDone(), is(false));

        final JSONObject userJson = jsonFuture.get();
        assertThat(userJson.containsKey("id"), is(true));
        assertThat(userJson.get("id"), is(USER_ID));
    }

    @Test
    public void getUserJson_With_Unknown_Id() throws ExecutionException, InterruptedException {
        final Future<JSONObject> jsonFuture = apiClient.getUser(UNKNOWN_USER_ID);
        assertThat(jsonFuture.isDone(), is(false));

        final JSONObject userJson = jsonFuture.get();
        assertThat(userJson.isEmpty(), is(true));
    }
}