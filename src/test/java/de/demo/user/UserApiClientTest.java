package de.demo.user;

import org.json.simple.JSONObject;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockserver.client.server.MockServerClient;
import org.mockserver.junit.MockServerRule;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static java.lang.String.format;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

public class UserApiClientTest {
    public static final String URL_TEMPLATE = "http://localhost:%s.com";
    public static final Long USER_ID = 111L;
    public static final String JSON_OBJECT_TEMPLATE = "{\"id\":%s}";

    public UserApiProperties properties = new UserApiProperties();
    public UserApiClient client;

    @Rule
    public MockServerRule mockServerRule = new MockServerRule(this);
    private MockServerClient mockServerClient =
            new MockServerClient("localhost", mockServerRule.getPort());

    @Before
    public void setup() {
        properties.setUrl(format(URL_TEMPLATE, mockServerRule.getPort()));
        client = new UserApiClient(properties);
        mockServerClient.reset();
    }

    @Test
    public void getUser() throws ExecutionException, InterruptedException {
        mockServerClient
                .when(request().withPath("/" + USER_ID.toString()).withMethod("GET"))
                .respond(response()
                        .withBody(format(JSON_OBJECT_TEMPLATE, USER_ID))
                        .withStatusCode(200));

        final CompletableFuture<JSONObject> userFuture = client.getUser(USER_ID);

        final JSONObject user = userFuture.get();
        assertThat(user.containsKey("id"), is(true));
        assertThat(user.get("id"), is(USER_ID));
    }
}