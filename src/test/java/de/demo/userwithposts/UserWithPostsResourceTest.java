package de.demo.userwithposts;

import com.google.common.collect.ImmutableMap;
import lombok.SneakyThrows;
import org.json.simple.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.concurrent.CompletableFuture;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;

public class UserWithPostsResourceTest {
	private static final long USER_ID = 4711L;

	private UserWithPostsService service;
	private UserWithPostsResource resource;
	private MockMvc mvc;

	@Before
	public void setup() {
		service = mock(UserWithPostsService.class);
		resource = new UserWithPostsResource(service);
		mvc = MockMvcBuilders.standaloneSetup(resource).build();
	}

	@Test
	@SneakyThrows
	public void getUserWithPosts() {

		final CompletableFuture<JSONObject> jsonFuture = new CompletableFuture<>();
		when(service.getUserWithPosts(USER_ID)).thenReturn(jsonFuture);

		jsonFuture.complete(createJSONObject());

		final MvcResult mvcResult = mvc.perform(get("/userwithposts/{userId}", USER_ID)).andExpect(request().asyncStarted()).andReturn();

		assertThat(mvcResult.getAsyncResult(), is(createJSONObject()));
		verify(service).getUserWithPosts(USER_ID);
	}

	private JSONObject createJSONObject() {
		return new JSONObject(ImmutableMap.of("key", "value"));
	}

}